\documentclass{beamer}

\usepackage{amssymb,amsmath,amsthm,amssymb,mathrsfs,setspace,pstcol,hyperref}
\usepackage[mathscr]{euscript}
\usepackage{play}
\usepackage{textcomp}
\usepackage{epsfig,epstopdf}
\usepackage{enumerate,setspace}
\usepackage{algorithm,algpseudocode}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\F}{\mathbb{F}}

\usetheme{PaloAlto}
\useoutertheme{infolines}

\begin{document}

\mode<presentation>
\title{PRIMALITY TESTING ALGORITHMS}
\author{Prerona Chatterjee\\(Roll No.: 142123029)}
\date{April, 2016}
\institute{Department of Mathematics \\IIT Guwahati}

\begin{frame}
	\titlepage
\end{frame}

\AtBeginSection[]
{
\begin{frame}
	\tableofcontents[currentsection]
\end{frame}
}

\section{The Problem}

\begin{frame}
\begin{problem}
Given an integer $n$ in binary representation, decides whether $n$ is a prime number in time $O(\log^k n)$ where $k$ is an integer independent of $n$. 
\end{problem}
\end{frame}

\section{Core Idea behind the Algorithms}

\begin{frame}
\begin{block}{The Core Idea}
\begin{enumerate}
\item If $n$ is less than some constant then check whether $n$ is prime by \emph{brute force}. Also, if $n=a^k$ for some integers $a$ and $k$ , where $k \geq 2$, then return COMPOSITE ({\sc isPower($n$)} shows than it can be checked efficiently). \pause
\item \emph{``Suitably choose"} a ring $R_n$ and a subset $S$ of $R_n$. \pause
\item For suitably chosen elements $a_1,\ldots,a_l\in S$, if $a_i$ does not satisfy a \emph{``special identity"} then return COMPOSITE (else we may need to perform some \emph{``other checks"}). \pause
\item If each $a_i$ satisfies the \emph{``special identity"} in $R_n$ then return PRIME.
\end{enumerate}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Miller-Rabin Test}
\begin{itemize}
\item $R_n=\Z_n$\pause
\item $S=\Z_n^*$ \pause
\item $a_1,\ldots,a_l$ are randomly chosen elements from $\Z_n^*$ \pause
\item The \emph{``special identity"} is $a_i^{n-1}=1$. \pause
\item Based on Fermat's little theorem: if $n$ is prime then for all $a\in \Z^*_n$, $a^{n-1}=1$.\pause
\item Tackles the issue of pseudoprime by checking for non-trivial square roots of unity \pause
\item It is probabilistic and exhibits one sided error
\end{itemize} 
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Agrawal-Biswas Test}
\begin{itemize}
\item $R_n=\Z_n[x]$ \pause
\item $S$ is the set of all monic polynomial of degree $\lceil\log n\rceil$ \pause
\item $Q(x)$ is randomly chosen from $S$ \pause
\item The \emph{``special identity"} is $(x+1)^n - (x^n+1) = 0$ modulo the polynomial $Q(x)$. \pause
\item Generalises the Fermat's little theorem to: $(x+a)^n = x^n+a$ if and only if $n$ is prime (where $a$ is any element from $\Z_n^*$) \pause
\item It reduces primality testing to polynomial identity testing, thus bypassing the pseudoprime issue \pause
\item It is probabilistic and exhibits one sided error
\end{itemize} 
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The AKS Test}
\begin{itemize}
\item $R_n=\Z_n/(x^r-1)$ where $r=O(\log^5n)$ \pause
\item $S=\{(x+a)^r-1\ |\ a=1,\ldots,l\}$ where $l=O(\log^5n)$\pause
\item The \emph{``special identity"} is $(x+1)^n - (x^n+1)$ is zero modulo $Q(x)$, $\forall Q(x)\in S$ \pause
\item It derandomises the Agrawal-Biswas test by reducing the size of $S$ from $O(n^{\log n})$ to $O(\log^5 n)$, allowing us to check the ``identity" exhaustively for all the elements in $S$ \pause
\item It is a deterministic algorithm
\end{itemize}
\end{block}
\end{frame}

\section{Preliminaries}

\begin{frame}
\begin{theorem}[Fermat's Little Theorem]\label{FLT} Let $p$ be a prime number. For all $a\in\F_p^*$, $a^{p-1}=1$.
\end{theorem} \pause
\begin{proof}
Since $p$ is prime, $\F_p^*=\F_p\setminus\{0\}$ and $|\F_p^*|=p-1$. Thus, for all $a\in \F_p^*$, $a^{|\F_p^*|}=a^{p-1}=1$.
\end{proof} \pause
The converse is not true. \pause
\begin{definition}
\begin{itemize}
\item A Fermat pseudoprime to base $a$ is a composite number $n$ such that $a^{n-1}=1$. For example, $a=2$ and $n=341$. \pause
\item A Carmichael number is an odd composite number $n$ for which $a^{n-1}=1$, $\forall a\in\F_p^*$. For example, $n = 561$.
\end{itemize}
\end{definition}
\end{frame}

\begin{frame}
\begin{theorem}[Generalisation of Fermat's Little Theorem] \label{AKS}
Let $P_{n}(x) = (a+x)^{n} - (a + x^{n})$ where $a \in \Z_{n}^{*}$ and $n \in \N$. Then, $P_{n}(x) = 0$ in $\Z_{n}[x]$ iff $n$ is prime.
\end{theorem} \pause
\begin{proof}
Let $n$ be prime. Then by Fermat\textquotesingle s little theorem, $a^n = a \pmod{n}$. Thus, $\forall j \in \{1,2,....,n-1\}, {n \choose j}$ is divisible by $n$ and so $P_n(x) = 0$ in $\Z_n[x]$ if $n$ is prime.\\
Conversely, if $n$ be composite, it has a non trivial prime divisor, $p$. The coefficient of $x^p$ = ${n \choose p} a^{n-p} \neq 0 \pmod{n}$, and so $P_n(x) \neq 0$ in $\Z_n[x]$.
\end{proof}
\end{frame}

\begin{frame}
\begin{theorem}[Chinese Remainder Theorem] \label{EQV}
Let $n\in \Z$ and $n = n_1n_2$ such that $gcd(n_1,n_2) = 1$, then $\Z_n$ is isomorphic to $\Z_{n_1} \times \Z_{n_2}$. In particular $\phi: \Z_n \rightarrow \Z_{n_1} \times \Z_{n_2}$ as defined below is an isomorphism:
\begin{center}
$\phi(x+n\Z) = (x+n_1\Z, x+n_2\Z)$.
\end{center}
Thus in particular, $\Z_n^*$ is isomorphic to $\Z_{n_1}^* \times \Z_{n_2}^*$
\end{theorem}
\end{frame}

\begin{frame}
\begin{block}{Some Basic Algorithms}\label{BAlgo}
\begin{itemize}
\item {\sc isPower($n$)} checks whether the integer $n$ can be expressed as $x^k$ where $x,k$ are integers and $k \geq 2$. Step 3 of {\sc isPower($n$)} can be done in $O(\log n)$ time and hence the total time complexity is $O(\log^2 n)$. \pause
\item {\sc gcd}$(a,b)$ computes the gcd of two integers using Euclid's method.  If $a$ and $b$ are $O(n)$, it can be shown that the while loop in {\sc gcd}$(a,b)$ iterates $O(\log n)$ times. \pause
\item {\sc Mod-Exp}$(a,b,n)$ gives an algorithm which takes time polynomial in $\log n$ for modular exponentiation. We cannot directly compute $a^b$ first and then take modulo $n$ because it will take $O(n\log n)$ bits to represent $a^b$ and hence the time will be superpolynomial in input size.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{algorithm}[H]
\caption{Check if a Number is an Integral Power}
{\fontsize{10}{10}\selectfont
\begin{algorithmic}[1]
\Procedure {isPower}{$n$}
\For {$k=1$ \textbf{to} $\lg n$}
\State Use bisection method to find the largest integer $x$ s.t. $x^{k} \leq n$
\If{$x^k=n$}
\State return True
\EndIf
\EndFor
\State return False
\EndProcedure
\end{algorithmic}}
\end{algorithm}
	
\vspace{-1em}

\begin{algorithm}[H]
\caption{Compute GCD of two integers}
{\fontsize{10}{10}\selectfont
\begin{algorithmic}[1]
\Procedure {gcd}{$a,b$}
\While {$b$ does not divide $a$}
\State $b'=  a \mod b$
\State $a=b$
\State $b=b'$
\EndWhile
\State return $b$
\EndProcedure
\end{algorithmic}}
\end{algorithm}
\end{frame}

\begin{frame}
\begin{algorithm}[H]
\caption{Modular Exponentiation: Given integers $a,b,n$ compute $a^b \mod n$}
\begin{algorithmic}[1]
\Procedure {Mod-Exp}{$a,b,n$}
\State $c=0$
\State $d=1$
\State let $\{b_{k}, b_{k-1},....,b_{0}\}$ be the binary representation of b
\For {i=k \textbf{downto} 0}
\State $c = 2c$
\State $d = d^{2}\pmod{n}$
\If{$b_{i} == 1$}
\State $c = c+1$
\State $d = d.a\pmod{n}$
\EndIf
\EndFor
\State return $d$
\EndProcedure
\end{algorithmic}
\end{algorithm}
\end{frame}

\section{The Miller-Rabin Primality Testing Algorithm}

\begin{frame}
\begin{block}{The Key Concepts}
Based on the Fermat's Little Theorem:
\begin{equation}\label{FLTEq}
a^{n-1}=1 \mbox{ } \forall a \in \Z_n^*
\end{equation} \pause
The 2 key concepts:	\pause
\begin{itemize}
\item If $n$ is not a Carmichael number, then the converse of equation \ref{FLTEq} is almost true. In particular, for at least half of the elements $a$ in $\Z_n^*$ equation \ref{FLTEq} does not hold.\pause
\item If $n$ is a Carmichael number, then with very high probability we can find a non trivial square root of unity in $\Z_n$.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Algorithm}
Let $n$ be the given number which we have to test for primality. \pause
\begin{itemize}
\item Check if $n$ is an integral power. Otherwise, rewrite $n-1$ as $2^tu$.
\pause
\item Pick some $a$ in the range $\{1, 2, ..., n-1\}$ and check whether {\sc gcd}$(a,n) = 1$ If not, return COMPOSITE \pause
\item Otherwise, check for non-trivial square-roots of unity in $\Z_n$ by first putting $x=a^u$ and then repeatedly squaring it till $x$ becomes equal to $a^{n-1}$ \pause
\item Check the Fermat\textquotesingle s condition, namely check whether $a^{n-1}=1$ for $a \in \Z_n^*$ \pause
\item If $n$ fails in either of the two tests, then return COMPOSITE. Otherwise return PRIME \pause
\item We can repeat the process a constant number of times by taking different $a$\textquotesingle s so as to decrease the probability of error
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{algorithm}[H]
\caption{Miller-Rabin Primality Test}
{\fontsize{10}{10}\selectfont
\begin{algorithmic}[1]
\Procedure {MillerRabin}{n}
\If{{\sc isPower($n$)} or $n$ is even with $n \neq 2$} \label{MR2}
\State return COMPOSITE
\EndIf
\State Choose $a$ to be a random number in the range $\{1,2,...,n-1\}$ 
\If{{\sc gcd}$(a,n) \neq 1$} \label{MR5}
\State return COMPOSITE
\EndIf 
\State Find $t$, $u$ such that $(n-1) = 2^{t}u$ and $u$ is odd \label{MR6}
\State Put $x_{0}$ = {\sc Mod-Exp$(a,u,n)$} \label{MR7}
\For {i=1 \textbf{to} t}
\State $x_{i} = x_{i-1}^{2}\pmod{n}$
\If{$x_{i} == 1$ and $x_{i-1} \neq 1$ and $x_{i-1} \neq -1$}
\State return COMPOSITE 
\State //Checking for non-trivial square roots of unity
\EndIf
\EndFor
\If{$x_{t} \neq 1$}
\State return COMPOSITE //Checking Fermat's Condition
\EndIf
\State return PRIME
\EndProcedure
\end{algorithmic}}
\end{algorithm}
\end{frame}

\begin{frame}
\begin{block}{Correctness}
\begin{itemize}
\item If $n$ is prime, then it can be trivially shown that the output is always correct. \pause
\item If $n$ is composite, we divide the analysis into two cases in both of which, we try to find a proper subgroup of $\Z_{n}^{*}$ which contains all the elements for which {\sc MillerRabin($n$)} returns PRIME and hence all the elements for which the algorithm can err.
\end{itemize} \pause
Thus, the error in the algorithm becomes less than $\frac{1}{2}$, and so can be made arbitrarily small by repeating the procedure a constant number of times.
\end{block}
\end{frame}

\begin{frame}
\begin{theorem}
If $n$ is prime, {\sc MillerRabin($n$)} outputs PRIME with probability one, and if $n$ is composite, it outputs COMPOSITE with probability $> \frac{1}{2}$.
\end{theorem} \pause
\begin{block}{Proof}
As explained in the key concepts, if $n$ is prime, then the output of the procedure {\sc MillerRabin($n$)} is always PRIME.\\ \pause
On the other hand, if $n$ is composite, we have the following two cases:\\
\pause
\textbf{Case 1}: $n$ is not a Carmichael number\pause
\begin{itemize}
\item Any $a$ that has reached line \ref{MR6} is in $\Z_n^*$ \pause
\item $\exists x \in \Z_{n}^{*}$ such that $x^{n-1} \neq 1 \pmod{n}$ \pause
\item $B = \{z \in \Z_{n}^{*} : z^{n-1} = 1 \pmod{n} \}$ is a proper non empty subgroup of $\Z_{n}^{*}$ \pause
\item $|B| \leq \frac{|\Z_{n}^{*}|}{2}$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Proof (contd...)}
\textbf{Case 2}: $n$ is a Carmichael number \pause
\begin{itemize}
\item Any $a$ that has reached line \ref{MR6} is in $\Z_n^*$ \pause
\item $\forall x \in \Z_{n}^{*}$, $x^{n-1} = 1 \pmod{n}$ \pause
\item $B = \{y \in \Z_{n}^{*} : y^u = \pm 1 \pmod{n} \}$ is a subgroup of $\Z_{n}^{*}$ \pause
\item For any $n$ such that the algorithm has reached \ref{MR5}, $n$ is not an integral power \pause
\item $\exists n_{1}, n_{2} \in \N$ such that $n = n_{1}n_{2}$ where $gcd(n_{1}, n_{2}) = 1$ and so by Theorem \ref{EQV}, $\Z_{n}^* \cong \Z_{n_1}^* \times \Z_{n_2}^*$ \pause
\item $\exists a \in \Z_n^* \backslash B$ such that $a \cong (1,-1)$ \pause
\item $B$ is a proper subgroup of $\Z_n^*$\pause
\item $|B| \leq \frac{|\Z_n^*|}{2}$
\end{itemize} 
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Proof (contd...)}
\begin{itemize}
\item For any `$a$' such that PRIME is returned by {\sc MillerRabin($n$)}, $a$ is a member of $B$ \pause
\item Any element in $\Z_n^*$ that is a non-witness to the compositeness of n is a member of $B$ \pause
\item The probability of error is $\leq \frac{|\Z_n^*|/2}{|\Z_n^*|} = \frac{1}{2}$ \pause
\item The output is COMPOSITE with probability $> \frac{1}{2}$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
	\begin{algorithm}[H]
		\caption{Miller-Rabin Primality Test}
		{\fontsize{10}{10}\selectfont
		\begin{algorithmic}[1]
			\Procedure {MillerRabin}{n}
			\If{{\sc isPower($n$)} or $n$ is even with $n \neq 2$} \label{MR2}
			\State return COMPOSITE
			\EndIf
			\State Choose $a$ to be a random number in the range $\{1,2,...,n-1\}$ 
			\If{{\sc gcd}$(a,n) \neq 1$} \label{MR5}
			\State return COMPOSITE
			\EndIf 
			\State Find $t$, $u$ such that $(n-1) = 2^{t}u$ and $u$ is odd \label{MR6}
			\State Put $x_{0}$ = {\sc Mod-Exp$(a,u,n)$} \label{MR7}
			\For {i=1 \textbf{to} t}
			\State $x_{i} = x_{i-1}^{2}\pmod{n}$
			\If{$x_{i} == 1$ and $x_{i-1} \neq 1$ and $x_{i-1} \neq -1$}
			\State return COMPOSITE 
			\State //Checking for non-trivial square roots of unity
			\EndIf
			\EndFor
			\If{$x_{t} \neq 1$}
			\State return COMPOSITE //Checking Fermat's Condition
			\EndIf
			\State return PRIME
			\EndProcedure
		\end{algorithmic}}
	\end{algorithm}
\end{frame}

\begin{frame}
\begin{block}{Running Time Analysis}
\begin{itemize}
\item If $n$ is the input, {\sc isPower($n$)} takes $O(\log^2 n)$ time and thus, Step \ref{MR2} takes $O(\log^2 n)$ time \pause
\item Step \ref{MR5} takes $O(\log n)$ time \pause
\item Step \ref{MR6} takes $O(\log n)$ time since $t$ (and hence $u$) can be found in atmost $\log n$ steps of dividing $(n-1)$ by $2$ \pause
\item  {\sc Mod-Exp}$(a,b,n)$ gives a polynomial time algorithm in $\log n$ for modular exponentiation as we have already seen in Section \ref{BAlgo} and thus Step \ref{MR7} takes time polynomial in $\log n$. \pause
\item Since no step in the for loop takes more that polynomial time in $\log n$, and since $t = O(\log n)$, {\sc Miller-Rabin($n$)} requires no more than time polynomial in $\log n$.
\end{itemize}
\end{block}
\end{frame}

\section{The Agrawal-Biswas Primality Testing Algorithm}

\begin{frame}
\begin{block}{The Key Concept}
\begin{itemize}
\item It is a randomised primality testing algorithm which reduces primality testing for a number $n$ to testing if a specific univariate identity over $\Z_{n}$ holds.\\ \pause
It uses the following generalisation of Fermat's little theorem over polynomial ring.
\begin{theorem} \label{AKS}
Let $P_{n}(x) = (a+x)^{n} - (a + x^{n})$ where $a \in \Z_{n}^{*}$ and $n \in \N$. Then, $P_{n}(x) = 0$ in $\Z_{n}[x]$ iff $n$ is prime.
\end{theorem} \pause
\item Unlike Fermat's little theorem, the identity is always false when $n$ is a composite number. \pause
\item The issue of pseudoprimes is completely sidestepped leading to the following simple algorithm: If $(x+1)^n=(x^n+1) \mod n$ then return PRIME else return COMPOSITE.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Key Concept (contd.)}
\begin{itemize}
\item The polynomials in both side expands into $O(n)$ term and we need $O(n)$ time to compute them. \pause
\item We check the identity modulo a randomly chosen monic polynomial of degree $\log n$ at the cost of introducing one sided error. \pause
\item The analysis of the algoritm shows that even in this case, when $n$ is COMPOSITE, the identity does not hold with probability greater than 2/3.
\end{itemize} 
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Algorithm}
\begin{itemize}
\item Check for trivial prime factors \pause
\item Choose $Q(x)$ randomly to be monic polynomial of degree $l = \lceil\log n\rceil$ \pause
\item Check whether $(x+1)^n - (x^n+1)$ is zero modulo the polynomial $Q(x)$
\pause
\item If that happens, return PRIME, and otherwise return COMPOSITE \pause
\item The process can be repeated a constant number of times by taking different $Q(x)$\textquotesingle s so as to decrease the probability of error arbitrarily.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{algorithm}[H]
\caption{Agrawal-Biswas Primality Test}
{\fontsize{10}{10}\selectfont
\begin{algorithmic}[1]
\Procedure {ABPrime}{$n$}
\If{$n = 2,3,5,7,11,13$}
\State return PRIME 
\Else 
\If{$n$ is divisible by any of the above numbers}
\State return COMPOSITE
\EndIf
\EndIf
\If{{\sc isPower($n$)}}\label{AB9}
\State return COMPOSITE
\EndIf
\State $P_n(x) = (1+x)^{n} - (1+x^n)$
\State Choose $Q(x)$ to be a random $l = \lceil\log n\rceil$ degree monic polynomial in $\Z_n[x]$
\If{$Q(x)$ divides $P_n(x)$ over $\Z_n$} \label{AB14}
\State return PRIME 
\Else
\State return COMPOSITE 
\EndIf
\EndProcedure
\end{algorithmic}}
\end{algorithm}
\end{frame}

\begin{frame}
\begin{block}{Correctness}
\begin{itemize}
\item If $n$ is prime, then it can be trivially shown that the output is always correct. \pause
\item However, if $n$ is composite, the probability that a monic polynomial $q$ with $deg(q) = l$ does not divide $P_n(x) > \frac{2}{3}$, so that the error in the algorithm becomes $\leq \frac{2}{3}$.
\end{itemize} \pause
To show this, we need the following distribution theorem of irreducible polynomials \cite{LN}:
\begin{center}
$\frac{p^{k}}{k} - p^{\frac{k}{2}} \leq I(k) \leq \frac{p^{k}}{k} + p^{\frac{k}{2}}$
\end{center}
where $I(k)$ be the number of monic irreducible polynomials of degree $k$ over $\F_{p}$
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Correctness (contd.)}
\begin{itemize}
\item Define a set $\mathcal{I}$ of monic irreducible polynomials having degree in the range $(\frac{l}{2}, l]$ over $\F_p$, where $p$ is a prime divisor of $n$. \pause
\item For each polynomial $f$ in $\mathcal{I}$, define a set $C_{f}$ of degree $l$ polynomials having $f$ as a factor. Then $\mid C_f \mid = p^{l-\mbox{deg(f)}} \leq p^{\frac{l}{2}-1}$ \pause
\item These $C_f$\textquotesingle s are mutually disjoint, and hence the number elements in $\displaystyle \cup_{f \in \mathcal{I}} C_f \geq (\ln 2 - \frac{1}{48})p^l$ \pause
\item The number of elements in $\mathcal{I}$ that divide $P_n(x) < \frac{2n}{l}$, and hence the number of elements in $\displaystyle \cup_{f \in \mathcal{I}} C_f$ that divide $P_n(x) < (\frac{2n}{l}) p^{\frac{l}{2}-1} \leq \frac{p^l}{8nl}$ \pause
\item The number of monic polynomials of degree $l$ with factors in $\mathcal{I}$ that do not divide $P_n(x) > (\ln 2 - \frac{1}{48} - \frac{1}{8nl})p^l > \frac{2}{3}p^l$.
\end{itemize} \pause
\end{block}
\end{frame}

\begin{frame}
We formalise this as follows:
\begin{theorem}
If $n$ is prime, {\sc ABPrime($n$)} outputs PRIME with probability one, and if $n$ is composite, its probability of error $\leq \frac{2}{3}$.
\end{theorem} \pause
\begin{block}{Running Time Analysis}
\begin{itemize}
\item $n$ is the input, {\sc isPower($n$)} takes $O(\log^2 n)$ time and thus, Step \ref{AB9} takes $O(\log^2 n)$ time \pause
\item In Step \ref{AB14}, the algorithm does $O(\log n)$ multiplications of two degree $O(\log n)$ polynomials over $\mathbb{Z}_{n}$ and computes same number of remainders modulo a third degree $O(\log n)$ polynomial and each of these requires $O^{\sim}(\log^3 n)$ \pause
\item Since these are the only two non trivial steps in the algorithm, the time complexity of the algorithm is $O^{\sim}(\log^4 n)$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
	\begin{algorithm}[H]
		\caption{Agrawal-Biswas Primality Test}
		{\fontsize{10}{10}\selectfont
		\begin{algorithmic}[1]
			\Procedure {ABPrime}{$n$}
			\If{$n = 2,3,5,7,11,13$}
			\State return PRIME 
			\Else 
			\If{$n$ is divisible by any of the above numbers}
			\State return COMPOSITE
			\EndIf
			\EndIf
			\If{{\sc isPower($n$)}}\label{AB9}
			\State return COMPOSITE
			\EndIf
			\State $P_n(x) = (1+x)^{n} - (1+x^n)$
			\State Choose $Q(x)$ to be a random $l = \lceil\log n\rceil$ degree monic polynomial in $\Z_n[x]$
			\If{$Q(x)$ divides $P_n(x)$ over $\Z_n$} \label{AB14}
			\State return PRIME 
			\Else
			\State return COMPOSITE 
			\EndIf
			\EndProcedure
		\end{algorithmic}}
	\end{algorithm}
\end{frame}

\begin{frame}
\begin{block}{Moving towards the AKS Algorithm}
\begin{enumerate}
\item The AKS algorithm is a derandomisation this algorithm. \pause
\item $S$ is the set of all monic polynomials of degree $\lceil\log n\rceil$ in $\mathbb{Z}_n$ and if $n$ is composite then there are at most $|S|/3$ many $Q(x)$'s in $S$ such that $P(x)=0 \mod Q(x)$. \pause
\item A naive approach to derandomize this algorithm is to check whether $P(x)=0 \mod Q(x)$ for more than $|S|/3$ many $Q(x)$. But this will take $O(|S|)$ time and $|S|=O(n^{\log n})$. \pause
\item In the AKS algorithm, we compute a $O(\log^5 n)$ integer $r$ and a $O(\log^5 n)$ integer $l$ s.t. if $n$ is composite then there exists an integer $a$ between $1$ and $l$ s.t. $P(x)\neq 0 \mod (x+a)^r-1$. \pause
\item The AKS algorithm does not use exactly this test. It checks whether for all $a$ between $1$ and $l$, $(x-a)^n=(x^n-a)$ over $\mathbb{Z}_n[x]/(x^r-1)$. 
\end{enumerate}
\end{block}
\end{frame}

\begin{frame}
The following lemma shows that these two tests are equivalent.
\begin{lemma}\label{LINK}
Fix any $r > 0$ and any $l > 0$. Then,
\begin{equation} \label{Eq1}
(x+1)^{n} = (x^{n} + 1) \pmod{n,(x+a)^{r}-1} \mbox{ for } 1 \leq a \leq l
\end{equation}
if and only if
\begin{equation} \label{Eq2}
(x-a)^{n} = (x^{n} - a) \pmod{n,x^{r}-1} \mbox{ for } 1 \leq a \leq l
\end{equation}
\end{lemma}
This can be easily proved using induction on $l$.
\end{frame}

\section{The AKS Primality Testing Algorithm}

\begin{frame}
\begin{block}{The Algorithm}
\begin{enumerate}
\item Determine whether the input is an integral power
\item Determine whether the input has a small prime divisor
\item Check whether $(x+a)^{n} = (x^{n}+a) \pmod{n, x^{r}-1}$.
\end{enumerate}\pause
The third step is equivalent to the condition that for all $Q(x)\in S = \{(x+a)^r-1\ |\ a=1,\ldots,l\}$,  $(x+1)^n - (x^n+1)$ is zero modulo $Q(x)$.
\end{block}
\end{frame}

\begin{frame}
\begin{algorithm}[H]
\caption{AKS Primality Test}
{\fontsize{10}{10}\selectfont
\begin{algorithmic}[1]
\Procedure {AKS}{$n$}
\If{isPower($n$)}
\State return COMPOSITE \label{AKS1}
\EndIf
\State Find the smallest $r$ such that $o_r(n) > 4\log^2 n$
\State Set $l = o_r(n) - 1$ //$o_r(n)$ is the order of $n$ modulo $r$
\If{$1 < gcd(a,n) < n$ for any $a \in \{1,2,...,r\}$}
\State return COMPOSITE \label{AKS4}
\EndIf
\If{$n\leq r$}
\State return PRIME \label{AKS5}
\EndIf
\For {$a=1$ \textbf{to} $l$}
\If{$(x+a)^n \neq (x^n+a) \pmod{n, x^{r}-1}$}
\State return COMPOSITE \label{AKS6}
\EndIf
\EndFor
\State return PRIME \label{AKS7}
\EndProcedure
\end{algorithmic}}
\end{algorithm}
\end{frame}

\begin{frame}
\begin{block}{Correctness}
\begin{itemize}
\item If $n$ is prime, then it can be trivially shown that the output is always correct \pause
\item For the converse, if AKS($n$) returns PRIME in step \ref{AKS5}, then it is easy to show that $n$ is prime \pause
\item If AKS($n$) returns PRIME in step \ref{AKS7}, then  we assume that $n$ is composite and that it has a non-trivial prime factor $p$ \pause
\item $F = \F_{p}[x]/<h(x)>$ where $h(x)$ is the irreducible part of the $r^{th}$ cyclotomic polynomial \pause
\item Consider $\mathcal{G} = <\{x+a : a \in \{1,2,...,l\}\}>$, which is a subgroup of $F^{*}$ where $l = o_{r}(n) - 1$ \pause
\item Contradictory bounds are derived for $\mid \mathcal{G} \mid$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{theorem}
If AKS($n$) returns PRIME in step \ref{AKS7}, then $n$ is prime.
\end{theorem} \pause

\begin{block}{Proof}
Suppose AKS($n$) returns PRIME in step \ref{AKS7}, and $n$ is composite. Then it will have a prime divisor $p$ and so we can consider $F$ as above \pause
\begin{itemize}
\item $G = <n,p>$ is a subgroup of $\Z_{r}^{*}$, and $t =$ $\mid G \mid$\\ \pause Note that $G \leq \Z_{r}^{*} \Rightarrow t < r$ and $<n>$ $\leq$ $<n,p> \Rightarrow o_{r}(n) < t$ \pause
\item $\mathcal{G} = <\{x+a : a \in \{1,2,...,l\}\}>$ is a subgroup of $F^{*}$ \pause
\item $m$ is said to be introspective for $f(x) \in F$ if $f(x^{m}) = f(x)^{m}$
\pause Note that $m$ is introspective for $f$, $\forall m \in G$ and $\forall f \in \mathcal{G}$ \pause Also, $m_{1}, m_{2}$ are introspective for $f$ $\Rightarrow m_{1}m_{2}$ is introspective for $f$ and $m$ is introspective for $f,g$ $\Rightarrow m$ is introspective for $fg$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Proof (contd.)}
\underline{Claim}: $\mid \mathcal{G} \mid$ $> 2^{t-1}$ \pause
\begin{itemize}
\item For each $K \subseteq \{1,2,...,l\}$, consider $f_{K}(x) = \prod_{a \in K} (x-a) \in \mathbb{Z}[x]$ \pause
\item Each $f_{K}$ is distinct since they have distinct roots \pause
\item There are $2^{l} > 2^{t-1}$ of such polynomials since there are $2^{l}$ subsets of ${1,2,...,l}$ \pause
\item In $F$, $x$ is the $r^{th}$ root of unity, $\zeta_{r}$, and so, $f_{K_{1}}(x) = f_{K_{2}}(x)$ in $F$ for $K_{1} \neq K_{2}$ would mean $f_{K_{1}}(\zeta_{r})^{m} = f_{K_{2}}(\zeta_{r})^{m}$ $\forall m \in G$ \pause
\item $\zeta_{r}^{m}$ is a root for $g=f_{K_{1}}-f_{K_{2}}$ $\forall m \in G$ where $g$ is a polynomial of degree $< t-1$ and $\mid G \mid =t$ \pause
\item This is not possible unless $f_{k_{1}} = f_{k_{2}}$ which is a contradiction \pause
\item $f_{K_{1}} \neq f_{K_{2}}$ in $F$ and hence in $\mathcal{G}$ for $K_{1} \neq K_{2}$  \pause $\Rightarrow \mid \mathcal{G} \mid$ $> 2^{t-1}$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Proof (contd.)}
\underline{Claim}: $\mid \mathcal{G} \mid$ $\leq n^{2\sqrt{t}}$ \pause
\begin{itemize}
\item $S = \{n^{i}p^{j},$ $0 \leq i,j \leq \sqrt{t}\}$ \pause
\item If $n$ is not a prime power, then each $n^{i}p^{j}$ is distinct, and hence $\mid S \mid$ $= (\sqrt{t}+1)^{2} > t$ \pause
\item Considering the elements of $S$ modulo $r$, they become elements of $G$ \pause
\item Since $\mid G \mid$ $= t$, $\exists m_{1}, m_{2}$ with $m_{1} \neq m_{2}$ such that $m_{1} = m_{2} \pmod{r}$ and so $m_{1} = m_{2} + rk$ for some $k \in \mathbb{Z}$ \pause
\item $\forall f \in \mathcal{G}$, $f(x)^{m_{1}} = f(x^{m_{1}}) = f(x^{m_{2}+rk}) = f(x^{m_{2}}x^{rk}) = f(x^{m_{2}}) = f(x)^{m_{2}}$ as $x^{r} = 1$ \pause
\item $f(x)$ is a root of $g=x^{m_{1}}-x^{m_{2}}$ $\forall f \in \mathcal{G}$ \pause
\item $deg(g) = max\{m_{1},m_{2}\} = n^{\sqrt{t}}p^{\sqrt{2}} < n^{\sqrt{t}}n^{\sqrt{t}} = n^{2\sqrt{t}}$ \pause
\item The number of roots of $g$ $= n^{2\sqrt{t}}$ and so $\mid \mathcal{G} \mid$ $\leq n^{2\sqrt{t}}$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Proof (contd.)}
\begin{itemize}
\item $2^{t-1} \leq$ $\mid \mathcal{G} \mid$ $\leq n^{2\sqrt{t}}$ \pause
\item As $t > 4\log^{2} n$, the bounds for $\mid \mathcal{G} \mid$ are contradictory
\end{itemize}  \pause
We thus have the required contradiction if we assume $n$ to be composite.
\end{block} \pause
\end{frame}

\begin{frame}
	\begin{algorithm}[H]
		\caption{AKS Primality Test}
		{\fontsize{10}{10}\selectfont
		\begin{algorithmic}[1]
			\Procedure {AKS}{$n$}
			\If{isPower($n$)}
			\State return COMPOSITE \label{AKS1}
			\EndIf
			\State Find the smallest $r$ such that $o_r(n) > 4\log^2 n$
			\State Set $l = o_r(n) - 1$ //$o_r(n)$ is the order of $n$ modulo $r$
			\If{$1 < gcd(a,n) < n$ for any $a \in \{1,2,...,r\}$}
			\State return COMPOSITE \label{AKS4}
			\EndIf
			\If{$n\leq r$}
			\State return PRIME \label{AKS5}
			\EndIf
			\For {$a=1$ \textbf{to} $l$}
			\If{$(x+a)^n \neq (x^n+a) \pmod{n, x^{r}-1}$}
			\State return COMPOSITE \label{AKS6}
			\EndIf
			\EndFor
			\State return PRIME \label{AKS7}
			\EndProcedure
		\end{algorithmic}}
	\end{algorithm}
\end{frame}

\begin{frame}
\begin{block}{Running Time Analysis}
Before we start the running time analysis, we need the following lemma:
\begin{lemma}\label{LCM}
Let LCM($m$) denote the lcm of the first $m$ numbers. For $m$ odd, LCM($m$) $\geq 2^{m-1}$.
\end{lemma} \pause
Using this, we can show the following result.
\begin{lemma}\label{TCAnal}
There exists an $r \leq 16\lg^{5}n$, such that $o_{r}(n) > 4\lg^{2} n$.
\end{lemma}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Running Time Analysis (contd.)}
\begin{itemize}
\item If $n$ is the input, {\sc isPower($n$)} takes $O(\log^2 n)$ time and so, step \ref{AKS1} takes $O(\log^2 n)$ time \pause
\item To find an $r$ such that $o_{r}(n) > 4\log^{2} n$, successive values of $r$ are tried and tested whether $n^{k} \neq 1 \pmod{r}$ for every $k \leq 4\log^{2} n$ \pause
\item For a particular $r$, this will involve at most $O(\log^{2} n)$ multipications modulo $r$ and so will take time $O^{\sim}(\log^{2} n \log r)$
\pause
\item Only $O(\log^{5} n)$ different $r$'s need to be checked, and so the total complexity of this step is polynomial in $\log n$ \pause
\item Each gcd computation takes time $O(\log^2 n)$ and since $r$ is of $O(\log^5 n)$, Step \ref{AKS4} will also be taking only time $O(\log^7 n)$ 
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Running Time Analysis (contd.)}
\begin{itemize}
\item We have to verify $l$ equations where $l = o_{r}(n)-1 < o_{r}(n)$ which divides $\phi(r)$ and hence, $l < \phi(r) < r$  is $O(\log^{5} n)$ \pause
\item Each equation takes $O(\log n)$ multipications of $r$ degree polynomials with coefficients of size $O(\log n)$ and so, each equation can be verified in time $O^{\sim}(r\log^{2} n)$ steps \pause
\item Thus the time taken for Step \ref{AKS6} is $O(r^2 \log^2 n) = O(log^{12} n)$. \pause
\item Step \ref{AKS6} dominates all the other steps and so, the time complexity for the AKS algorithm is $O(log^{12} n)$ which is polynomial in $\log n$
\end{itemize}
\end{block} \pause
\begin{block}{Note}
The time complexity of this algorithm can be improved slightly, the current best being about $O(\log^{6} n)$
\end{block}
\end{frame}

\section{References}

\nocite{MIL} \nocite{RAB} \nocite{AB} \nocite{AKS} \nocite{LN}
\bibliographystyle{plain}
\bibliography{bib}

\begin{frame}
\begin{center}
\begin{huge}
\textbf{Thankyou}
\end{huge}
\end{center}
\end{frame}

\end{document} 